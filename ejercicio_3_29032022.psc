Algoritmo ejercicio_3_29032022
	Definir numeroEntero Como Entero
	Definir modificador como Entero
	Definir auxiliarIncremiento Como Entero
	Definir auxiliarDecremento Como Entero
	
	
	numeroEntero = 1000
	modificador = 50
	
	auxiliarIncremento = numeroEntero + (numeroEntero * modificador / 100)
	auxiliarDecremento = numeroEntero - (numeroEntero * modificador / 100)
	
	Escribir "el total de " numeroEntero " incrementado en " modificador " % es de :"
	Escribir auxiliarIncremento
	Escribir "el total de " numeroEntero " decrementado en en " modificador " % es de :"
	Escribir auxiliarDecremento

FinAlgoritmo
